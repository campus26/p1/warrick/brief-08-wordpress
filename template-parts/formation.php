<main>
        <section class="formation" id="formation">
            <h2>MES FORMATIONS</h2>
            <div>
                <div>
                    <div data-aos="fade-right">
                        <div>
                            <h4>HTML</h4>
                            <p id="phtml">Création template, mise en place de divers blocs,
                                modifications ou ajouts d'interfaces,
                                création de formulaire, mise en place d'informations et/ou objets graphiques et
                                multimédias</p>
                        </div>
                        <img id="ihtml" src="<?php echo get_template_directory_uri();?>/images/html.png">
                    </div>
                    <div data-aos="fade-left">
                        <img id="iphp" src="<?php echo get_template_directory_uri();?>/images/logo2.png">
                        <div>
                            <h4>PHP</h4>
                            <p id="pphp">Utilisation courante pour la création de projet avec WAMP ou dans un CMS. Structuration
                                du code via MVC et routeur.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div data-aos="fade-right">
                        <div>
                            <h4>CSS</h4>
                            <p id="pcss">Gestion d'appellation d'élément HTML avec mise en page. Mise en place de quelques
                                animations (animation, transform, box-shadow, rotation). Gestion flex box, media query
                                et autre élément responsive.</p>
                        </div>
                        <img id="icss" src="<?php echo get_template_directory_uri();?>/images/css-3.svg">
                    </div>
                    <div data-aos="fade-left">
                        <img id="isql" src="<?php echo get_template_directory_uri();?>/images/sql-server.svg">
                        <div>
                            <h4>SQL</h4>
                            <p id="psql">Acquis système CRUD avec les requêtes SQL ainsi que la gestion d'un formulaire.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div data-aos="fade-right">
                        <div>
                            <h4>JavaScript</h4>
                            <p id="pjs">Créer une fonction, des variables, Gestion de module,
                                installer des librairies (SASS / Matter.js) et en faire l'usage.
                            </p>
                        </div>
                        <img id="ijs" src="<?php echo get_template_directory_uri();?>/images/52497.png">
                    </div>
                    <div data-aos="fade-left">
                        <img id="imsql" src="<?php echo get_template_directory_uri();?>/images/mysql.svg">
                        <div>
                            <h4>MySQL</h4>
                            <p id="pmsql">Utilisation basique de l'interface de commande.</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div data-aos="fade-right">
                        <div>
                            <h4>REACT</h4>
                            <p id="preact">Pas encore vu! Apprentissage dans les mois à venir.</p>
                        </div>
                        <img id="ireact" src="<?php echo get_template_directory_uri();?>/images/physics.svg">
                    </div>
                    <div data-aos="fade-left">
                        <img id="isf" src="<?php echo get_template_directory_uri();?>/images/logo1.png">
                        <div>
                            <h4>SYMFONY</h4>
                            <p id="psf">Pas encore vu! Apprentissage dans les mois à venir.</p>
                        </div>
                    </div>
                </div>
                <?php get_template_part( 'template-parts/section/section-3' );?>
            </div>
        </section>