<section class="competences" id="competences">
            <h2>MES COMPETENCES</h2>
            <div>
                <div data-aos="fade-down" data-aos-duration="1500">
                    <h2>Domaines de compétences FRONT-END</h2>
                    <h4>MAQUETTER UNE APPLICATION</h4>
                    <p>Je sais reproduire une maquette à l'identique ainsi que sa charte graphique.</p>

                    <h4>REALISER UNE INTERFACE STATIQUE ET ADAPTABLE</h4>
                    <p>J'adapte le contenu de l'interface au besoin ainsi que la mise en forme.
                        J'apporte des changements majeurs à la structure de l'interface, j'ajoute des pages à mon
                        interface,
                        des règles de mise en forme.</p>

                    <h4>DEVELOPPER UNE INTERFACE DYNAMIQUE</h4>
                    <p>A partir du besoin exprimé, je réalise des modifications majeures (ajout de champs côté client),
                        j'ajoute des pages dynamiques,
                        j'ajoute des tests des traitements côté client.</p>

                    <h4>REALISER UNE INTERFACE DE GESTION DE CONTENUE</h4>
                    <p>À partir d'un site existant, j'installe une solution de type CMS avec des
                        modifications via le Back-office.</p>
                </div>
                <div data-aos="fade-down" data-aos-duration="1500">
                    <h2>Domaines de compétences BACK-END</h2>
                    <h4>CREATION DE BASE DE DONNEES</h4>
                    <p>A partir d'un schéma physique de données, je créer une base de données sur un SGBD avec
                        création d'utilisateur pour la base et leur attribuer des droits spécifiés ainsi que la
                        création d'un script d'insertion de données.</p>

                    <h4>DEVELOPPER DES COMPOSANTS D'ACCES AUX DONNEES</h4>
                    <p>A partir d'une base de données,
                        j'adapte les requêtes de sélections pour requêter mes données,
                        j'adapte les requêtes d'insertion ou de mise à jour de mes données,
                        J'adapte les requêtes de suppressions.
                    </p>

                    <h4>DEVELOPPER LA PARTIE BACH-END D'UNE APPLICATION</h4>
                    <p>A partir d'un back-end existant, je suis en mesure d'apporter des modifications mineures aux
                        traitements réalisés côté serveur.
                        Il peut notamment s'agir des traitements liés à la collecte, au traitement ou à la restitution
                        de données.</p>

                </div>

            </div>
        </section>