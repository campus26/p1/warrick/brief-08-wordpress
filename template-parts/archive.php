<?php get_header()?>

<?php get_template_part( 'template-parts/header' );?>
  
<?php get_template_part( 'template-parts/menu' );?>

<?php get_template_part( 'template-parts/sidebar-header' );?>

<?php get_template_part( 'template-parts/footer' );?>

<?php get_footer()?>