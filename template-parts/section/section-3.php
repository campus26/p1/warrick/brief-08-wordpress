<div class="wrapper row2">
  <main class="hoc pos_center clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="sectiontitle">
      <h6 class="heading">Mes Formateurs</h6>
      <p>Apprentissage Des Bases De Codage</p>
    </div>
    <div class="group cont_font center ">
      <article class="form_third left_0"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Anthony</h6>
      </article>
      <article class="form_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Kristen</h6>
      </article>
      <article class="form_third"><a class="ringcon btmspace-50" href="#"><i class="fas fa-address-book"></i></a>
        <h6 class="heading">Alexandre</h6>
      </article>
      <p class="center"><a class="btn" href="#">INACTIF<i class="fas fa-angle-right"></i></a></p>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>