<div class="wrapper row1">
  <section class="hoc clear"> 
    <!-- ################################################################################################ -->
   
    <nav id="mainav">
      <ul class="clear">
        <li class="active"><a href="<?php echo get_template_directory_uri();?>index.html">Home</a></li>
        <li><a class="drop" href="<?php echo get_template_directory_uri();?>">Pages</a>
          <ul>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/gallery.html">Gallery</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/full-width.html">Full Width</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/sidebar-left.html">Sidebar Left</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/sidebar-right.html">Sidebar Right</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/basic-grid.html">Basic Grid</a></li>
            <li><a href="<?php echo get_template_directory_uri();?>/pages/font-icons.html">Font Icons</a></li>
          </ul>
        </li>
        <li><a class="drop" href="<?php echo get_template_directory_uri();?>">Dropdown</a>
          <ul>
            <li><a href="<?php echo get_template_directory_uri();?>">Level 2</a></li>
            <li><a class="drop" href="<?php echo get_template_directory_uri();?>">Level 2 + Drop</a>
              <ul>
                <li><a href="<?php echo get_template_directory_uri();?>">Level 3</a></li>
                <li><a href="<?php echo get_template_directory_uri();?>">Level 3</a></li>
                <li><a href="<?php echo get_template_directory_uri();?>">Level 3</a></li>
              </ul>
            </li>
            <li><a href="<?php echo get_template_directory_uri();?>#">Level 2</a></li>
          </ul>
        </li>
        <li><a href="<?php echo get_template_directory_uri();?>">Link Text</a></li>
        <li><a href="<?php echo get_template_directory_uri();?>">Link Text</a></li>
        <li><a href="<?php echo get_template_directory_uri();?>">Link Text</a></li>
        <li><a href="<?php echo get_template_directory_uri();?>">Long Link Text</a></li>
      </ul>
    </nav>
    <!-- ################################################################################################ -->
    <div id="searchform">
      <div>
        <form action="<?php echo get_template_directory_uri();?>" method="post">
          <fieldset>
            <legend>Quick Search:</legend>
            <input type="text" placeholder="Enter search term&hellip;">
            <button type="submit"><i class="fas fa-search"></i></button>
          </fieldset>
        </form>
      </div>
    </div>
    <!-- ################################################################################################ -->
  </section>
</div>