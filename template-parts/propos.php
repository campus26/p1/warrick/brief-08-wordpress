<section class="propos" id="propos">
            <h2>A PROPOS</h2>
            <h3>Je suis apprenti développeur web et je souhaite évoluer dans ce domaine!</h3>
            <div>
                <div data-aos="fade-down-right">
                    <h4>Un apprenti passionné, une expérience enrichie!</h4>
                    <p>Après plusieurs expériences professionnelles dans plusieurs domaines, le développement web est
                        celle qui me plait le plus.</p>
                    <p>J'ai commencé à m'y intéresser en janvier 2020, après avoir réaliser mon projet montage PC en
                        2019 et j'ai débuté avec YouTube dans la création d'un CV qui fut mes premières lignes de code.
                    </p>
                    <p>En Octobre 2020, j'ai eu la chance de pouvoir intégré la formation initiatique au métier de
                        développeur web proposer par Campus26, où j'ai pu découvrir et apprendre plusieurs languages (
                        HTML / CSS / JavaScript / PHP ) ainsi qu'une nouvelle façon d'apprendre avec la pédagogie
                        active.</p>
                </div>
                <div>
                    <img src="<?php echo get_template_directory_uri();?>/images/DSC09150test1.jpg">
                </div>
                <div data-aos="fade-down-right">
                    <h4>En formation!</h4>
                    <p>Aujourd'hui, toujours avec la pédagogie active au sein de la formation "Développeur Web et Web
                        mobile", j'approfondis mes connaissances dans le développement web avec l'apprentissage de
                        nouveaux languages et l'amélioration de mes acquis lors de la précédante formation.</p>
                    <p>De janvier 2021 à octobre de cette année, je poursuits mon évolution dans divers projets à l'aide
                        de mes camarades apprenants, de nos formateurs et de Campus26 by Simplon afin de pouvoir
                        m'élancer dans ce milieu professionnel.</p>
                    <p>Au bout de 3,5 mois de formation, nous avons pu découvrir divers voies où sépanouir,
                        l'utilisation multiple de certains languages ainsi que leurs compréhensions et leurs buts
                        d'utilisation lors de la réalisation de nos projets.</p>
                </div>
            </div>
        </section>
    </main>