
<div class="sectiontitle">
    <h6 class="heading">Mes derniers article</h6>
    <p>Dolor in fermentum ipsum vel mi mattis venenatis vivamus</p>
</div>
<div id="latest" class="group">

<?php
 
// The Query
$the_query = new WP_Query( 
    array(
        'category_name' => 'news'
    )
 );

// The Loop
if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) { ?>


       <?php $the_query->the_post(); ?>
         <article id="goliath" class="one_third">
             <a class="imgover" href="<?php echo get_permalink();?>"><img src="<?php echo get_the_post_thumbnail_url(); ?>"></a>
         <div class="excerpt">
             
                  <h6 class="heading"><?php echo get_the_title();?></h6>
    
            
            <footer><a href="<?php echo get_permalink();?>">Read More <i class="fas fa-angle-right"></i></a></footer>
        </div>
           </article>
    <?php 
    }
} 
wp_reset_postdata();?>
  
