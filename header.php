<!DOCTYPE html>
<html lang="fr">

<head>
<title><?php the_title()?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/framework.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/stylemenu.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
</head>

<body>
    <?php wp_body_open(); ?>